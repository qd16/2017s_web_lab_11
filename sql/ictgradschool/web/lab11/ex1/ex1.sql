-- Answers to exercise 1 questions
-- A.
SELECT DISTINCT dept FROM unidb.unidb_courses;

-- B.
SELECT DISTINCT semester FROM unidb_attend;

-- C.
SELECT DISTINCT dept, num FROM unidb_attend;

-- D.
SELECT fname, lname, country FROM unidb_students
  ORDER BY fname;

-- E.
SELECT fname, lname, mentor FROM unidb_students
  ORDER BY mentor;

-- F.
SELECT fname, lname FROM unidb_lecturers
  ORDER BY office;


--