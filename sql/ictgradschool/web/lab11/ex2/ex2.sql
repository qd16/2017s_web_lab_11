-- Answers to exercise 2 questions
-- A.
SELECT unidb_students.fname, unidb_students.lname, stu.id FROM unidb_attend AS stu JOIN unidb_students
  ON stu.id = unidb_students.id
  WHERE dept='comp' AND num=219;

-- B.
SELECT DISTINCT unidb_students.fname, unidb_students.lname FROM unidb_attend
  JOIN unidb_students
  ON unidb_attend.id = unidb_students.id
  WHERE NOT unidb_students.country='NZ';

-- C.
SELECT office FROM unidb_lecturers JOIN unidb_courses
  ON unidb_courses.coord_no = unidb_lecturers.staff_no
  WHERE unidb_courses.num=219;

-- D.
SELECT unidb_students.fname, unidb_students.lname FROM unidb_courses
  JOIN unidb_lecturers
  ON unidb_courses.coord_no = unidb_lecturers.staff_no
  JOIN unidb_students
  ON unidb_courses.rep_id = unidb_students.id
  WHERE unidb_lecturers.fname='Te Taka';

-- E.
SELECT s.lname as mentoree, m.lname as mentor
FROM unidb_students s, unidb_students m
  WHERE  s.mentor=m.id;



-- F.
SELECT l.lname AS lname, l.fname as fname FROM unidb_lecturers l
  WHERE l.office LIKE 'G%'
UNION
SELECT s.lname , s.fname  FROM unidb_students s
  WHERE NOT s.country='NZ';

-- G.

SELECT
       CONCAT(l.fname, ' ', l.lname) AS coordinator,
       CONCAT(s.fname, ' ', s.lname) AS representative
  FROM
       unidb_courses AS c,
       unidb_lecturers AS l,
       unidb_students AS s
  WHERE
       c.rep_id = s.id AND c.coord_no = l.staff_no -- Our joins
       AND
       c.dept = 'COMP' AND c.num=219;
 -- JOIN unidb_students ON c.rep_id = unidb_students.id
  -- WHERE c.dept='comp' AND c.num=219


